//
// Game.cpp
//

#include "pch.h"
#include "Game.h"
#include "GameObject.h"
#include "GameController.h"
#include "Collider.h"
extern void ExitGame() noexcept;

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Game::Game() noexcept(false)
{
	m_deviceResources = std::make_unique<DX::DeviceResources>();
	m_deviceResources->RegisterDeviceNotify(this);
}

// Initialize the Direct3D resources required to run.
void Game::Initialize(HWND window, int width, int height)
{
	m_deviceResources->SetWindow(window, width, height);

	m_deviceResources->CreateDeviceResources();
	CreateDeviceDependentResources();

	m_deviceResources->CreateWindowSizeDependentResources();
	CreateWindowSizeDependentResources();

	// TODO: Change the timer settings if you want something other than the default variable timestep mode.
	// e.g. for 60 FPS fixed timestep update logic, call:
   /*
	m_timer.SetFixedTimeStep(true);
	m_timer.SetTargetElapsedSeconds(1.0 / 60);
   */
	m_keyboard = std::make_unique<Keyboard>();
	m_mouse = std::make_unique<Mouse>();
	m_mouse->SetWindow(window);
	GameController::GetInstance().setCenter(m_center);
	GameController::GetInstance().restartGame();
}

#pragma region Frame Update
// Executes the basic game loop.
void Game::Tick()
{
	m_timer.Tick([&]()
		{
			Update(m_timer);
		});

	Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
	float elapsedTime = float(timer.GetElapsedSeconds());

	// TODO: Add your game logic here.
   // elapsedTime;
	auto kb = m_keyboard->GetState();
	m_keys.Update(kb);

	if (kb.Escape)
	{
		ExitGame();
	}
	if (kb.R) {
		GameController::GetInstance().restartGame();
	}
	if (GameController::GetInstance().getNumOfBricks() > 0 && GameController::GetInstance().getNumOfLifes() > 0) {
		for (GameObject* gameObject : GameController::GetInstance().getObjects()) {
			gameObject->Update(elapsedTime, kb);
		}
	}
}
#pragma endregion

#pragma region Frame Render
// Draws the scene.
void Game::Render()
{
	// Don't try to render anything before the first Update.
	if (m_timer.GetFrameCount() == 0)
	{
		return;
	}

	Clear();

	m_deviceResources->PIXBeginEvent(L"Render");
	auto context = m_deviceResources->GetD3DDeviceContext();
	context->OMSetBlendState(m_states->Opaque(), nullptr, 0xFFFFFFFF);
	context->OMSetDepthStencilState(m_states->DepthNone(), 0);
	context->RSSetState(m_states->CullNone());

	m_effect->Apply(context);

	context->IASetInputLayout(m_inputLayout.Get());

	m_batch->Begin();

	for (const GameObject* gameObject : GameController::GetInstance().getObjects()) {
		gameObject->Draw(m_batch.get());
	}
	for (const Collider* collider : GameController::GetInstance().getGameColliders(GameController::CollisionTag::wall)) {
		collider->Draw(m_batch.get());
	}

	m_batch->End();

	m_spriteBatch->Begin();

	std::string briks = "Briks left:" + std::to_string(GameController::GetInstance().getNumOfBricks());
	char const* nBriks = briks.c_str();
	Vector2 originBriks = m_font->MeasureString(nBriks) / 2;
	m_font->DrawString(m_spriteBatch.get(), nBriks, DirectX::SimpleMath::Vector2(110.0f, 25.0f), Colors::White, 0.f, originBriks);

	std::string lifes = "Lifes left:" + std::to_string(GameController::GetInstance().getNumOfLifes());
	char const* nLifes = lifes.c_str();
	Vector2 originLifes = m_font->MeasureString(nLifes) / 2;
	m_font->DrawString(m_spriteBatch.get(), nLifes, DirectX::SimpleMath::Vector2(680.0f, 25.0f), Colors::White, 0.f, originLifes);

	if (GameController::GetInstance().getNumOfBricks() == 0) {
		const wchar_t* wonMessage = L"You Won! Press R to restart.";
		Vector2 origin = m_font->MeasureString(wonMessage) / 2;
		m_font->DrawString(m_spriteBatch.get(), wonMessage, m_fontPos, Colors::White, 0.f, origin);
	}
	if (GameController::GetInstance().getNumOfLifes() == 0) {
		std::string temp = "Game over! Number of lifes:" + std::to_string(GameController::GetInstance().getNumOfLifes()) + "\n     Press R to restart.";
		char const* message = temp.c_str();
		Vector2 origin = m_font->MeasureString(message) / 2;
		m_font->DrawString(m_spriteBatch.get(), message, m_fontPos, Colors::White, 0.f, origin);

	}

	m_spriteBatch->End();

	m_deviceResources->PIXEndEvent();
	// Show the new frame.
	m_deviceResources->Present();

	m_graphicsMemory->Commit();
}

// Helper method to clear the back buffers.
void Game::Clear()
{
	m_deviceResources->PIXBeginEvent(L"Clear");

	// Clear the views.
	auto context = m_deviceResources->GetD3DDeviceContext();
	auto renderTarget = m_deviceResources->GetRenderTargetView();
	auto depthStencil = m_deviceResources->GetDepthStencilView();

	context->ClearRenderTargetView(renderTarget, Colors::Black);
	context->ClearDepthStencilView(depthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	context->OMSetRenderTargets(1, &renderTarget, depthStencil);

	// Set the viewport.
	auto viewport = m_deviceResources->GetScreenViewport();
	context->RSSetViewports(1, &viewport);

	m_deviceResources->PIXEndEvent();
}
#pragma endregion

#pragma region Message Handlers
// Message handlers
void Game::OnActivated()
{
	// TODO: Game is becoming active window.
	m_keys.Reset();
	m_mouseButtons.Reset();
}

void Game::OnDeactivated()
{
	// TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
	// TODO: Game is being power-suspended (or minimized).
}

void Game::OnResuming()
{
	m_timer.ResetElapsedTime();
	m_keys.Reset();
	m_mouseButtons.Reset();
	// TODO: Game is being power-resumed (or returning from minimize).
}

void Game::OnWindowMoved()
{
	auto r = m_deviceResources->GetOutputSize();
	m_deviceResources->WindowSizeChanged(r.right, r.bottom);
}

void Game::OnWindowSizeChanged(int width, int height)
{
	if (!m_deviceResources->WindowSizeChanged(width, height))
		return;

	CreateWindowSizeDependentResources();

	// TODO: Game window is being resized.
}

// Properties
void Game::GetDefaultSize(int& width, int& height) const noexcept
{
	// TODO: Change to desired default window size (note minimum size is 320x200).
	width = 800;
	height = 600;
}
#pragma endregion

#pragma region Direct3D Resources
// These are the resources that depend on the device.
void Game::CreateDeviceDependentResources()
{
	auto device = m_deviceResources->GetD3DDevice();
	m_states = std::make_unique<CommonStates>(device);
	// TODO: Initialize device dependent objects here (independent of window size).
	//device;
	m_graphicsMemory = std::make_unique<GraphicsMemory>(device);
	auto context = m_deviceResources->GetD3DDeviceContext();
	m_spriteBatch = std::make_unique<SpriteBatch>(context);
	m_effect = std::make_unique<BasicEffect>(device);
	m_batch = std::make_unique<PrimitiveBatch<DirectX::VertexPositionColor>>(context);
	m_font = std::make_unique<SpriteFont>(device, L"myfile.spritefont");
	m_effect->SetVertexColorEnabled(true);

	DX::ThrowIfFailed(
		CreateInputLayoutFromEffect<DirectX::VertexPositionColor>(device, m_effect.get(),
			m_inputLayout.ReleaseAndGetAddressOf())
	);
}

// Allocate all memory resources that change on a window SizeChanged event.
void Game::CreateWindowSizeDependentResources()
{
	auto size = m_deviceResources->GetOutputSize();
	m_center.x = float(size.right) / 2.0f;
	m_center.y = float(size.bottom) / 2.0f;
	m_fontPos.x = m_center.x;
	m_fontPos.y = m_center.y;
	Matrix proj = Matrix::CreateScale(2.f / float(size.right),
		-2.f / float(size.bottom), 1.f)
		* Matrix::CreateTranslation(-1.f, 1.f, 0.f);

	m_effect->SetProjection(proj);

}

void Game::OnDeviceLost()
{
	// TODO: Add Direct3D resource cleanup here.
	m_graphicsMemory.reset();
	m_states.reset();
	m_effect.reset();
	m_batch.reset();
	m_font.reset();
	m_spriteBatch.reset();
	m_inputLayout.Reset();
}

void Game::OnDeviceRestored()
{
	CreateDeviceDependentResources();

	CreateWindowSizeDependentResources();
}
#pragma endregion
