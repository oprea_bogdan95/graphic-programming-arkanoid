#pragma once
#include "GameObject.h"
class Collider;

class Ball :public GameObject {
private:
	float _speed = 300.0f;
	bool _isXSwitched = false;
	bool _isYSwitched = false;
public:
	Ball(Vector2 position);
	virtual void Update(float i_deltaTime, const KeyboardState& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;
	virtual void OnCollisionStart(Collider* obj1, Collider* obj2) override;
};