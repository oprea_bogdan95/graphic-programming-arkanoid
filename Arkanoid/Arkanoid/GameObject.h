#pragma once

class Collider;
class GameObject {
protected:
	static constexpr float MIN_VELOCITY = 0.001f;

	using Vector2 = DirectX::SimpleMath::Vector2;
	Vector2 _position;
	Vector2 _size;
	Vector2 _velocity;
	DirectX::XMVECTORF32 _color;
	std::vector<Collider*> _colliders;
public:
	GameObject();
	GameObject(Vector2 position);
	GameObject(Vector2 position, Vector2 size);
	GameObject(Vector2 position, Vector2 size, DirectX::XMVECTORF32 color);
	using KeyboardState = DirectX::Keyboard::State;
	virtual void Update(float detaTime, const KeyboardState& keyboard);
	virtual void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch)const;
	std::vector<Collider*> getColliders();
	virtual void OnCollisionStart(Collider* obj1, Collider* obj2);
	virtual void OnCollisionStay(Collider* obj1, Collider* obj2);
	Vector2 getPosition();
	Vector2 getSize();
	Vector2 getVelocity();

	void setVelocity(Vector2 velocity);
	void setPosition(Vector2 position);
	virtual ~GameObject();
};