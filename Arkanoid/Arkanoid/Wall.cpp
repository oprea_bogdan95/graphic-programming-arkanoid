#include "pch.h"
#include "Wall.h"
#include "GameController.h"
#include "Collider.h"
Wall::Wall(Vector2 position, Vector2 size) : GameObject(position, size, DirectX::Colors::Transparent)
{
	float colliderHalfSize = 25.0f;
	float extraOffset = 10.0f;

	_colliders.push_back(new Collider(this, GameController::CollisionTag::wall, Vector2(-(_size.x / 2) - colliderHalfSize, 0.0f), Vector2(colliderHalfSize, (_size.y / 2) + extraOffset) * 2.0f));
	_colliders.push_back(new Collider(this, GameController::CollisionTag::wall, Vector2(0.0f, -(_size.y / 2) - colliderHalfSize), Vector2((_size.x / 2) + extraOffset, colliderHalfSize) * 2.0f));
	_colliders.push_back(new Collider(this, GameController::CollisionTag::wall, Vector2((_size.x / 2) + colliderHalfSize, 0.0f), Vector2(colliderHalfSize, (_size.y / 2) + extraOffset) * 2.0f));
	_colliders.push_back(new Collider(this, GameController::CollisionTag::deathZone, Vector2(0.0f, (_size.y / 2) + colliderHalfSize), Vector2((_size.x / 2) + extraOffset, colliderHalfSize) * 2.0f));
}

void Wall::Update(float deltaTime, const KeyboardState& keyboard)
{
	GameObject::Update(deltaTime, keyboard);
}

void Wall::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::Draw(batch);
}
