#pragma once
#include "GameObject.h"
class Wall :public GameObject {
public:
	Wall(Vector2 position, Vector2 size);
	virtual void Update(float i_deltaTime, const KeyboardState& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;
};