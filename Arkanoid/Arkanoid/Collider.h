#pragma once
#include "GameController.h"
class GameObject;
class Collider {
protected:
	GameController::CollisionTag _collisionTag;
	GameObject* _owner;
	using Vector2 = DirectX::SimpleMath::Vector2;
	Vector2 _size;
	Vector2 _position;
	std::vector<Collider*>_collisions;
	std::vector<Collider*>_newCollisions;
	std::vector<Collider*>_oldCollisions;
	std::vector<Collider*>_temporaryCollisions;
	void updateCollisions();
public:
	Collider();
	Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position, Vector2 size);
	void Update();
	void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const;
	void setCollisionTag(GameController::CollisionTag collisionTag);
	GameController::CollisionTag getCollisionTag();
	void setOwner(GameObject* owner);
	GameObject* getOwner();
	bool canCollideWith(Collider* other);
	DirectX::SimpleMath::Rectangle getWorldRectangle();
};