#include "pch.h"
#include "Collider.h"
#include "GameObject.h"

void Collider::updateCollisions()
{
	if (!_newCollisions.empty()) {
		_newCollisions.clear();
	}

	if (!_oldCollisions.empty()) {
		_oldCollisions.clear();
	}
	if (!_temporaryCollisions.empty()) {
		_temporaryCollisions.clear();
	}
	for (Collider* collider : GameController::GetInstance().getGameColliders(_collisionTag)) {
		if (collider->canCollideWith(this)) {
			_temporaryCollisions.push_back(collider);
			bool found = false;
			for (Collider* oldCollision : _collisions) {
				if (oldCollision == collider) {
					found = true;
					break;
				}
			}
			if (found) {
				_oldCollisions.push_back(collider);
			}
			else {
				_newCollisions.push_back(collider);
			}
		}
	}

	for (Collider* prevCollider : _collisions) {
		bool found = false;
		for (Collider* oldCollider : _oldCollisions) {
			if (prevCollider == oldCollider) {
				found = true;
				break;
			}
		}
	}

	if (!_collisions.empty()) {
		_collisions.clear();
	}

	for (Collider* collision : _temporaryCollisions) {
		_collisions.push_back(collision);
	}
}

Collider::Collider() :_owner(nullptr), _collisionTag(GameController::wall), _position(0.0f, 0.0f), _size(0.0f, 0.0f)
{
}

Collider::Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position, Vector2 size) : _owner(owner), _collisionTag(collisionTag), _position(position), _size(size)
{
	_collisions = std::vector<Collider*>();
	_newCollisions = std::vector<Collider*>();
	_oldCollisions = std::vector<Collider*>();
	_temporaryCollisions = std::vector<Collider*>();
}

void Collider::Update()
{
	updateCollisions();

	if (!_newCollisions.empty()) {
		for (Collider* collider : _newCollisions) {
			_owner->OnCollisionStart(this, collider);
		}
	}

	if (!_oldCollisions.empty()) {
		for (Collider* collider : _oldCollisions) {
			_owner->OnCollisionStay(this, collider);
		}
	}
}

void Collider::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{

	float padding = 2.5f;
	DirectX::XMVECTORF32 color = DirectX::Colors::BlanchedAlmond;

	DirectX::VertexPositionColor v1(Vector2(_owner->getPosition().x + _position.x - _size.x / 2 + padding, _owner->getPosition().y + _position.y - _size.y / 2 + padding), color);
	DirectX::VertexPositionColor v2(Vector2(_owner->getPosition().x + _position.x + _size.x / 2 - padding, _owner->getPosition().y + _position.y - _size.y / 2 + padding), color);
	DirectX::VertexPositionColor v3(Vector2(_owner->getPosition().x + _position.x + _size.x / 2 - padding, _owner->getPosition().y + _position.y + _size.y / 2 - padding), color);
	DirectX::VertexPositionColor v4(Vector2(_owner->getPosition().x + _position.x - _size.x / 2 + padding, _owner->getPosition().y + _position.y + _size.y / 2 - padding), color);

	batch->DrawQuad(v1, v2, v3, v4);
}

void Collider::setCollisionTag(GameController::CollisionTag collisionTag)
{
	_collisionTag = collisionTag;
}

GameController::CollisionTag Collider::getCollisionTag()
{
	return _collisionTag;
}

void Collider::setOwner(GameObject* owner)
{
	_owner = owner;
}

GameObject* Collider::getOwner()
{
	return _owner;
}

bool Collider::canCollideWith(Collider* other)
{
	return !DirectX::SimpleMath::Rectangle::Intersect(this->getWorldRectangle(), other->getWorldRectangle()).IsEmpty();
}

DirectX::SimpleMath::Rectangle Collider::getWorldRectangle()
{
	return DirectX::SimpleMath::Rectangle(_owner->getPosition().x + _position.x - _size.x / 2, _owner->getPosition().y + _position.y - _size.y / 2, _size.x, _size.y);
}
