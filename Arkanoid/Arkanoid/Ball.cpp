#include "pch.h"
#include "Ball.h"
#include "Collider.h"
#include "GameController.h"
#include <random>
#define M_PI  3.14159265358979323846f
Ball::Ball(Vector2 position) :GameObject(position, Vector2(15.0f, 15.0f), DirectX::Colors::Orange)
{
	_colliders.push_back(new Collider(this, GameController::CollisionTag::ball, Vector2(0.0f, 0.0f), _size));
	setVelocity(Vector2::Transform(Vector2(0.0f, -1.0f), DirectX::SimpleMath::Matrix::CreateRotationZ(88.0f)) * _speed);
}

void Ball::Update(float deltaTime, const KeyboardState& keyboard)
{
	_isXSwitched = false;
	_isYSwitched = false;
	GameObject::Update(deltaTime, keyboard);
}

void Ball::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::Draw(batch);
}

void Ball::OnCollisionStart(Collider* obj1, Collider* obj2)
{
	GameObject::OnCollisionStart(obj1, obj2);

	if (obj2->getCollisionTag() == GameController::CollisionTag::deathZone) {
		setVelocity(Vector2(0.0f, 0.0f));
		if (GameController::GetInstance().getNumOfLifes() < 0) {
			GameController::GetInstance().gameOver();
		}
		else {
			for (Collider* collider : _colliders) {
				if (collider != nullptr) {
					delete collider;
					collider = nullptr;
				}
			}
			_colliders.clear();
			_color = DirectX::Colors::Transparent;
			GameController::GetInstance().decrementLives();
			GameController::GetInstance().restartBall();
		}
	}
	else {
		bool isXSwitched = false, isYSwitched = false;
		DirectX::SimpleMath::Rectangle intersection = DirectX::SimpleMath::Rectangle::Intersect(obj1->getWorldRectangle(), obj2->getWorldRectangle());
		if (intersection.width >= intersection.height && !_isYSwitched) {
			isYSwitched = true;
			_isYSwitched = true;
		}
		else if (!_isXSwitched) {
			isXSwitched = true;
			_isXSwitched = true;
		}
		if (isXSwitched) {
			// cambio direzione della velocita lungo x
			setVelocity(Vector2(-getVelocity().x, getVelocity().y));
		}
		if (isYSwitched) {
			// cambio direzione della velocita lungo y 
			setVelocity(Vector2(getVelocity().x, -getVelocity().y));
		}

		if (obj2->getCollisionTag() == GameController::CollisionTag::paddle) {
			float devAngle = (_position.x - obj2->getOwner()->getPosition().x) / obj2->getOwner()->getSize().x / 2;
			devAngle *= 75.0f;

			devAngle = devAngle * M_PI / 180.0f;
			setVelocity(Vector2::Transform(Vector2(0.0f, -1.0f),DirectX::SimpleMath::Matrix::CreateRotationZ(devAngle)) * _speed);
		}
	}
}

