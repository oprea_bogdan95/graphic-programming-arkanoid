#pragma once

class GameObject;
class Collider;
class GameController {
private:
	GameController();
	int _numerOfBricks;
	int _numberOfLifes;
	int _rows = 3;
	int _columms = 8;
	using Vector2 = DirectX::SimpleMath::Vector2;
	Vector2 _center;
	std::vector<GameObject*> _gameObjects;
	bool _collisionMatrix[5][5] = {
		{false, true,false, true,false},
		{ true,false,false, true,false},
		{false,false,false, true,false},
		{ true, true, true,false, true},
		{false,false,false, true,false}
	};
public:
	enum CollisionTag {
		wall, paddle, brick, ball, deathZone
	};
public:
	GameController(const GameController&) = delete;
	GameController& operator=(const GameController&) = delete;
	static GameController& GetInstance();
	std::vector<Collider*> getGameColliders(CollisionTag tag);
	void addObject(GameObject* gameObject);
	std::vector<GameObject*> getObjects();
	void setNumOfBricks(int numberOfBriks);
	int getNumOfBricks();
	void setNumOfLifes(int numberOfLifes);
	int getNumOfLifes();
	void decrementNumOfBriks();
	void decrementLives();
	void gameOver();
	void restartGame();
	void restartBall();
	void setCenter(Vector2);
};