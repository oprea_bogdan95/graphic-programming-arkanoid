#include "pch.h"
#include "GameController.h"
#include "GameObject.h"
#include "Collider.h"
#include "Wall.h"
#include "Paddle.h"
#include "Brick.h"
#include "Ball.h"

GameController::GameController() : _numerOfBricks(0), _numberOfLifes(0)
{
}

GameController& GameController::GetInstance()
{
	static GameController instance;
	return instance;
}

std::vector<Collider*> GameController::getGameColliders(CollisionTag tag)
{
	std::vector<Collider*> colliders = std::vector<Collider*>();
	for (GameObject* gameObject : _gameObjects) {
		for (Collider* collider : gameObject->getColliders()) {
			if (_collisionMatrix[tag][collider->getCollisionTag()]) {
				colliders.push_back(collider);
			}
		}
	}
	return colliders;
}

void GameController::addObject(GameObject* gameObject)
{
	_gameObjects.push_back(gameObject);
}

std::vector<GameObject*> GameController::getObjects()
{
	return _gameObjects;
}

void GameController::setNumOfBricks(int numberOfBriks)
{
	_numerOfBricks = numberOfBriks;
}

int  GameController::getNumOfBricks()
{
	return _numerOfBricks;
}

void GameController::setNumOfLifes(int numberOfLifes)
{
	_numberOfLifes = numberOfLifes;
}

int GameController::getNumOfLifes()
{
	return _numberOfLifes;
}

void GameController::decrementNumOfBriks()
{
	_numerOfBricks--;
}

void GameController::decrementLives()
{
	_numberOfLifes--;
}

void GameController::gameOver()
{
	if (_numberOfLifes > 0) {
		return;
	}
	for (GameObject* gameObject : _gameObjects) {
		if (gameObject != nullptr) {
			delete gameObject;
			gameObject = nullptr;
		}
	}
	_gameObjects.clear();
	setNumOfBricks(0);
}

void GameController::restartGame()
{
	for (GameObject* gameObject : _gameObjects) {
		if (gameObject != nullptr) {
			delete gameObject;
			gameObject = nullptr;
		}
	}
	_gameObjects.clear();
	Vector2 centerOfBrick = _center + Vector2(0.0f, -200.0f);
	setNumOfLifes(3);
	addObject(new Wall(_center, Vector2(780.0f, 580.0f)));
	addObject(new Paddle(_center + Vector2(0.0f, 200.0f)));
	addObject(new Ball(_center + Vector2(0.0f, 180.0f)));
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _columms; j++) {
			addObject(new Brick(centerOfBrick + Vector2(-280.0f + j * 80.0f, 0.0f + i * 30.0f + 20.0f * i)));
		}
	}
	setNumOfBricks(_rows * _columms);
}

void GameController::restartBall()
{
	addObject(new Ball(_center + Vector2(0.0f, 180.0f)));
}

void GameController::setCenter(Vector2 center)
{
	_center = center;
}

